import ROOT
import array
import itertools
from collections import defaultdict,OrderedDict
from array import array
from collections import defaultdict

def MakeSourceTree(rawdatafilename,rootfile):

    print "[SourceTree] parsing data file " + rawdatafilename

    nlines = len(open(rawdatafilename, "r").readlines())

    f = open(rawdatafilename, "r")

    rootfile.cd()

    peary_tree=ROOT.TTree("Hits","peary data tree")

    maxn=10000
    col = array('i',maxn*[0])
    row = array('i',maxn*[0])
    ts1 = array('i',maxn*[0])
    ts2 = array('i',maxn*[0])
    tot = array('L',maxn*[0])
    BinCnt = array('i',maxn*[0])
    trigger_cnt = array('i',[0])
    fpga_ts = array('L',[0])
    nhits = array('i',[0])
    Timing = array('i',maxn*[0])

    peary_tree.Branch('NHits',nhits,"nhits/I")
    peary_tree.Branch('PixX',col,"col[nhits]/I")
    peary_tree.Branch('PixY',row,"row[nhits]/I")
    peary_tree.Branch('ts1',ts1,"ts1[nhits]/I")
    peary_tree.Branch('ts2',ts2,"ts2[nhits]/I")
    peary_tree.Branch("tot",tot,"tot[nhits]/L")
    peary_tree.Branch('trigger_cnt',trigger_cnt,"trigger_cnt/I")
    peary_tree.Branch('fpga_ts',fpga_ts,"fpga_ts/L")
    peary_tree.Branch('BinCnt',BinCnt,"BinCnt[nhits]/I")
    peary_tree.Branch('Timing',Timing,"Timing[nhits]/I")

    unsorted_data = []



    for line in f :
        if "HIT" in line:
            words = line.split()
            hit = [int(word) for word in words[1:]]
            #print hit
            col[0] = hit[0]
            row[0] = hit[1]
            ts1[0] = hit[2]
            ts2[0] = hit[3]
            tot[0] = hit[4]
            BinCnt[0] = hit[6]
            Timing[0] = hit[8]
            nhits[0]=1
            peary_tree.Fill()

    peary_tree.Write()
    rootfile.Write()

def plotTOT(rootfile):
    pearytree = rootfile.Get("Hits")
    pearytree.Draw("tot >> h(64,0,64)")

    XY = ROOT.TH2D("TOTMap","TOTMap",25,0,25,400,0,400)
    TOT ={}
    N = {}
    N=defaultdict(lambda : 0,N)
    TOT=defaultdict(lambda : 0,TOT)
    for entry in pearytree:
        TOT["%i %i"%(entry.PixX[0],entry.PixY[0])]+=entry.tot[0]
        N["%i %i"%(entry.PixX[0],entry.PixY[0])]+=1




    for col in range(25):
        for row in range(400):
            if(N["%i %i"%(col,row)]!=0):
                TOT["%i %i"%(col,row)] /=N["%i %i"%(col,row)]
                XY.Fill(col,row,TOT["%i %i"%(col,row)])

    pearytree.Draw("PixX:PixY:Sum$(tot )/Length$(tot ) >> h2(25,0,25,400,0,400)")
    return rootfile.Get("h"),XY

